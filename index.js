/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/
   
 
        function register(AddUser) {

            let result = registeredUsers.includes(AddUser);

                   
                 if (AddUser == null || AddUser == "") {

                     }
                 else if (result){

                      alert('Registration failed. Username already exists!');

                      }
                    else 
                    { 

                       registeredUsers[registeredUsers.length] = AddUser;
                        alert("Thank you for registering!");
                    }
        }
        register()

/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/

        
    function addFriend(addfriend) {

        
       let result = registeredUsers.includes(addfriend);
               
             if (addfriend == null || addfriend == "") {

              }
             else if (result){

                    friendsList[friendsList.length] = addfriend;
                  alert('You have added <registeredUser> as a friend!');

                }
                else {
                  

                    alert("User not found.!");
                }
        }
    
    addFriend()



/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/
     

    function friendList(friends){
             
             friends = friendsList.length;

            if (friends <= 0){

                alert("You currently have 0 friends. Add one first.");

            } else {

                console.log(friendsList);
            }

    }


   
    function displyFriends(friendnum){
             
             friendnum = friendsList.length;

            if (friendnum <= 0){

                alert("You currently have 0 friends. Add one first.");

            } else {

                console.log(friendsList[friendnum]);
            }

    }

/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/
   
function displayNumberOfFriends(numberoffriends){

            numberoffriends = friendsList.length;

            if(numberoffriends > 1){

                    alert("You currently have " + numberoffriends + " friends");
                   

                } else {

                    alert("You currently have " + numberoffriends + " friends. Add one first.");
                 

            }

        }

        displayNumberOfFriends()
  
/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/

    function deleteFriend(){

              let friend = friendsList.length;

               if (friend >= 1){
                        
                   friendsList.length-- 

               } else{

                alert("You currently have 0 friends. Add one first.");

               }
         }


/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/

    function userSplice(num1){
                  
            friendsList.length -1;
            friendsList.splice(num1);
    
            }
    userSplice();



